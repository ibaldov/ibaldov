<p align="center">
<img width="256" alt="ISO C++ Logo" src="https://upload.wikimedia.org/wikipedia/commons/thumb/1/18/ISO_C%2B%2B_Logo.svg/256px-ISO_C%2B%2B_Logo.svg.png">

<img width="256" alt="Go Logo Blue" src="https://upload.wikimedia.org/wikipedia/commons/thumb/0/05/Go_Logo_Blue.svg/256px-Go_Logo_Blue.svg.png">


<img width="256" alt="Bash Logo Colored" src="https://bashlogo.com/img/logo/svg/full_colored_light.svg">
</p>
